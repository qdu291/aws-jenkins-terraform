terraform {
  backend "s3" {
    bucket = "node-aws-jenkins-terraform-qdu291-1"
    key = "node-aws-jenkins-terraform.tfstate"
    region = "us-east-1"
  }
}